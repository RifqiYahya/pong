using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    [SerializeField] KeyCode upButton;
    [SerializeField] KeyCode downButton;
    [SerializeField] float speed = 10f;
    [SerializeField] float yBoundary = 9f;
    private Rigidbody2D rb;
    public int score = 0;

    private ContactPoint2D lastContactPoint;
    public ContactPoint2D LastContactPoint { get { return lastContactPoint; } }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            lastContactPoint = collision.GetContact(0);
        }
    }

    public void incrementScore()
    {
        score++;
    }

    public void resetScore()
    {
        score = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = transform.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 velocity = rb.velocity;
        if (Input.GetKey(upButton)) {
            velocity.y = speed;
        } else if (Input.GetKey(downButton)) {
            velocity.y = -speed;
        } else {
            velocity.y = 0;
        }

        rb.velocity = velocity;

        Vector3 position = transform.position;

        if(position.y > yBoundary) {
            position.y = -yBoundary;
        } else  if (position.y < -yBoundary) {
            position.y = yBoundary;
        }

        transform.position = position;
    }
}
