using UnityEngine;

public class WallControl : MonoBehaviour
{
    [SerializeField] PlayerControl player;
    [SerializeField] BallControl ball;
    [SerializeField] GameManager gameManager;

    void OnTriggerEnter2D (Collider2D collider)
    {
        if (collider.name == "Ball")
        {
            player.incrementScore();
            if(player.score < gameManager.maxScore)
            {
                ball.restartBall();
            }
        }
    }
}
