using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] float respawnDelay = 2.0f;
    float xInitialForce = 5.0f;
    [SerializeField] float forceMagnitude = 1000.0f;
    [SerializeField] float yInitialForce = 300.0f;

    private Vector2 trajectoryOrigin;
    public Vector2 TrajectoryOrigin { get { return trajectoryOrigin;  } }

    void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }

    private void pushBall() {
        float yRandomInitalForce = Random.Range(-yInitialForce, yInitialForce);
        float randomDirection = Random.Range(0,2);

        xInitialForce = Mathf.Sqrt(forceMagnitude*forceMagnitude - yRandomInitalForce*yRandomInitalForce);

        if (randomDirection < 1)
        {
            rb.AddForce(new Vector2(-xInitialForce, yRandomInitalForce));
        } else
        {
            rb.AddForce(new Vector2(xInitialForce, yRandomInitalForce));
        }
    }

    public void resetBall() {
        transform.position = Vector2.zero;
        rb.velocity = Vector2.zero;
    }

    public void restartBall()
    {
        resetBall();
        Invoke("pushBall", respawnDelay);
    }


    // Start is called before the first frame update
    void Start()
    {
        rb = transform.GetComponent<Rigidbody2D>();
    }
}
