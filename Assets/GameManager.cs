using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] PlayerControl player1;
    Rigidbody2D player1rb;

    [SerializeField] PlayerControl player2;
    Rigidbody2D player2rb;

    [SerializeField] BallControl ball;
    Rigidbody2D ballrb;
    CircleCollider2D ballCollider;
    bool firstLaunch = true;

    public int maxScore = 5;

    bool isDebugWindowShown = false;

    [SerializeField] Trajectory trajectory;

    // Start is called before the first frame update
    void Start()
    {
        player1rb = player1.GetComponent<Rigidbody2D>();
        player2rb = player2.GetComponent<Rigidbody2D>();

        ballrb = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
    }

    void OnGUI()
    {
        string debugText =
            "Ball mass = " + ballrb.mass + "\n" +
            "Ball velocity = " + ballrb.velocity + "\n" +
            "Ball speed = " + ballrb.velocity.magnitude + "\n" +
            "Ball momentum = " + ballrb.mass * ballrb.velocity + "\n" +
            "Ball friction = " + ballCollider.friction + "\n" +
            "Last impulse from player 1 = (" + player1.LastContactPoint.normalImpulse + "," + player1.LastContactPoint.tangentImpulse + ")\n" +
            "Last impulse from player 2 = (" + player2.LastContactPoint.normalImpulse + "," + player2.LastContactPoint.tangentImpulse + ")\n";


        GUI.Label(new Rect(Screen.width / 2 - 150 - 12, 20, 100, 100), "" + player1.score);
        GUI.Label(new Rect(Screen.width / 2 + 150 + 12, 20, 100, 100), "" + player2.score);

        if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), firstLaunch? "START" : "RESTART"))
        {
            player1.resetScore();
            player2.resetScore();

            ball.restartBall();
            firstLaunch = false;
        }

        if (player1.score >= maxScore)
        {
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "PLAYER 1 WINS");

            ball.resetBall();

        } else if (player2.score >= maxScore)
        {
            GUI.Label(new Rect(Screen.width / 2 + 150, Screen.height / 2 - 10, 2000, 1000), "PLAYER 2 WINS");

            ball.resetBall();
        }

        if (isDebugWindowShown)
        {
            Color oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.red;
            GUIStyle guiStyle = new GUIStyle(GUI.skin.textArea);
            guiStyle.alignment = TextAnchor.UpperCenter;
            GUI.TextArea(new Rect(Screen.width / 2 - 200, Screen.height - 200, 400, 110), debugText, guiStyle);

            GUI.backgroundColor = oldColor;
        }

        if (GUI.Button(new Rect(Screen.width/2 - 60, Screen.height - 73, 120, 53), "TOGGLE \nDEBUG INFO"))
        {
            isDebugWindowShown = !isDebugWindowShown;
            trajectory.enabled = !trajectory.enabled;
        }

    }
}
